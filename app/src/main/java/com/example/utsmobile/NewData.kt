package com.example.utsmobile

fun tambahData(): List<ProdiData> {
    val partList = ArrayList<ProdiData>()
    partList.add(
        ProdiData(
            "Fakultas Ekonomi dan Bisnis",
            "Fakultas Ekonomi dan Bisnis merupakan salah satu dari 7 Fakultas di UPN “Veteran” Jawa Timur. Yang terdiri dari program studi:",
            "1. Prodi S1 Ekonomi Pembangunan\n" +
                    "2. Prodi S1 Akuntansi\n" +
                    "3. Prodi S1 Manajemen",
            R.drawable.upn,
            "feb@upnjatim.ac.id",
            "http://febis.upnjatim.ac.id/"
        )
    )
    partList.add(
        ProdiData(
            "Fakultas Pertanian",
            "Fakultas Pertanian merupakan salah satu dari 7 Fakultas di UPN “Veteran” Jawa Timur. Yang terdiri dari program studi:",
            "1. Prodi S1 Agroteknologi\n" +
                    "2. Prodi S1 Agribisnis",
            R.drawable.upn,
            "",
            "http://faperta.upnjatim.ac.id/"
        )
    )
    partList.add(
        ProdiData(
            "Fakultas Teknik",
            "Fakultas Teknik merupakan salah satu dari 7 Fakultas di UPN “Veteran” Jawa Timur. Yang terdiri dari program studi:",
            "1. Prodi S1 Teknik Kimia\n" +
                    "2. Prodi S1 Teknik Industri\n" +
                    "3. Prodi S1 Teknik Sipil\n" +
                    "4. Prodi S1 Teknik Lingkungan\n" +
                    "5. Prodi S1 Teknologi Pangan",
            R.drawable.upn,
            "",
            "http://ft.upnjatim.ac.id/"
        )
    )
    partList.add(
        ProdiData(
            "Fakultas Ilmu Sosial dan Ilmu Politik",
            "Fakultas Ilmu Sosial dan Ilmu Politik merupakan salah satu dari 7 Fakultas di UPN “Veteran” Jawa Timur. Yang terdiri dari program studi:",
            "1. Prodi S1 Ilmu Komunikasi\n" +
                    "2. Prodi S1 Ilmu Administrasi Bisnis\n" +
                    "3. Prodi S1 Ilmu Administrasi Negara\n" +
                    "4. Prodi S1 Hubungan Internasional",
            R.drawable.upn,
            "",
            "http://fisip.upnjatim.ac.id/"
        )
    )
    partList.add(
        ProdiData(
            "Fakultas Ilmu Komputer",
            "Fakultas Ilmu Komputer merupakan salah satu dari 7 Fakultas di UPN “Veteran” Jawa Timur. Yang terdiri dari program studi:",
            "1. Prodi S1 Teknik Informatika\n" +
                    "2. Prodi S1 Sistem Informasi\n" +
                    "3. Prodi S1 Data Sains",
            R.drawable.upn,
            "",
            "https://fik.upnjatim.ac.id/"
        )
    )
    partList.add(
        ProdiData(
            "Fakultas Arsitektur dan Desain",
            "Fakultas Arsitektur dan Desain merupakan salah satu dari 7 Fakultas di UPN “Veteran” Jawa Timur. Yang terdiri dari program studi:",
            "1. Prodi S1 Teknik Arsitektur\n" +
                    "2. Prodi S1 Desain Komunikasi Visual",
            R.drawable.upn,
            "fad@upnjatim.ac.id",
            "http://fad.upnjatim.ac.id/"
        )
    )
    partList.add(
        ProdiData(
            "Fakultas Hukum",
            "Fakultas Ilmu Komputer merupakan salah satu dari 7 Fakultas di UPN “Veteran” Jawa Timur. Yang terdiri dari program studi:",
            "1. Prodi S1 Hukum",
            R.drawable.upn,
            "",
            "http://fhukum.upnjatim.ac.id/"
        )
    )
    partList.add(
        ProdiData(
            "Profil Mahasiswa",
            "Mahasiswa Fakultas Ilmu Komputer SI17, UPN VETERAN JATIM",
            "Biodata:\n" +
                    "1.\tNama \t: Afifah Mahardika\n" +
                    "2.\tTTL\t: Surabaya, 17 Agustus 1999\n" +
                    "3.\tAlamat \t: Jl. Setro Baru Utara 9/62\n" +
                    "4.\tNo. HP \t: 082228901236\n",
            R.drawable.am,
            "afifahmahardikaa@gmail.com",
            "https://bitbucket.org/afifahm/uts-17082010050/src/master/"
        )
    )
    return partList
}